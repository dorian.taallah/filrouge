# TP Le Grand Restaurant
## Auteurs
- Dorian Taallah : [dorian-taallah](https://gitlab.com/dorian.taallah)
- Jean-Maurice Raboude : [JMRG28](https://gitlab.com/JMRG28)
- Lucas Leroux : [Leroux Lucas]()
- Louis Palot : [endrelou](https://gitlab.com/Endrelou)
- Ramzi Zeroual : [RamziZer](https://gitlab.com/RamziZer)
- Yoann Lecanu : [yoann](https://gitlab.com/isil34)

## Première Itération
Branche : [master](https://gitlab.com/dorian.taallah/filrouge)

## Deuxième Itération - Tests

Nous avons utilisé 6 différents frameworks d’assertions (1 par branche) : 
- [master - Jupiter](https://gitlab.com/dorian.taallah/filrouge)
- [ramzi - Truth](https://gitlab.com/dorian.taallah/filrouge/-/tree/ramzi)
- [JM - valid4j](https://gitlab.com/dorian.taallah/filrouge/-/tree/JM)
- [Assert_Louis - hamcrest](https://gitlab.com/dorian.taallah/filrouge/-/tree/Assert_Louis)
- [lucas - assertj](https://gitlab.com/dorian.taallah/filrouge/-/tree/lucas)
- [yo - TestNG](https://gitlab.com/dorian.taallah/filrouge/-/tree/yo)


## Troisième Itération
Branche : [IT3](https://gitlab.com/dorian.taallah/filrouge/-/tree/IT3) 

## Dernière Itération
Branche : [JM](https://gitlab.com/dorian.taallah/filrouge/-/tree/JM) 

Pour lancer l'API , il faut lancer le projet, directement dans l'IDE ou en récupérant le build et en le lancant avec : 
Lien vers le dernier build :    
[Release](https://gitlab.com/dorian.taallah/filrouge/-/releases)  
[Build](https://gitlab.com/dorian.taallah/filrouge/-/jobs/2182637114/artifacts/browse/target)
```sh
java -jar <chemin_vers_jar>
```

ensuite aller sur le port 9000 en local : 
```sh
localhost:9000
localhost:9000/commandes
localhost:9000/restaurant
```
Pour la CI/CD nous avons utilisé un workflow gitlab que vous pouvez trouver ici : [https://gitlab.com/dorian.taallah/filrouge/-/blob/JM/.gitlab-ci.yml](https://gitlab.com/dorian.taallah/filrouge/-/blob/JM/.gitlab-ci.yml)

Qui fait que pour chaque push sur la branche JM , le code sera compilé, les tests seront fait et il sera ensuite déployé.

