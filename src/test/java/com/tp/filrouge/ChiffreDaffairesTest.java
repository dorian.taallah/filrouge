package com.tp.filrouge;

import com.tp.filrouge.model.Commande;
import com.tp.filrouge.model.Franchise;
import com.tp.filrouge.model.Restaurant;
import com.tp.filrouge.model.Serveur;
import com.tp.filrouge.model.builder.*;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class ChiffreDaffairesTest {


    // Serveur


    @Test
    @DisplayName("ÉTANT DONNÉ un nouveau serveur " +
            "QUAND on récupére son chiffre d'affaires " +
            "ALORS celui-ci est à 0")
    void getCaNouveauServeur() {
        // ÉTANT DONNÉ un nouveau serveur
        Serveur serveur = new ServeurBuilder().build();

        // QUAND on récupére son chiffre d'affaires
        double chiffreAffaires = serveur.getChiffreAffaires();

        // ALORS celui-ci est à 0
        assertEquals(0, chiffreAffaires);
    }

    @Test
    @DisplayName("ÉTANT DONNÉ un nouveau serveur " +
            "QUAND il prend une commande " +
            "ALORS son chiffre d'affaires est le montant de celle-ci")
    void getCaServeurUneCommande() {
        // ÉTANT DONNÉ un nouveau serveur
        Serveur serveur = new ServeurBuilder().build();

        // QUAND il prend une commande
        Commande commande = new CommandeBuilder()
                            .montant(10)
                            .isNourriture(true)
                            .build();
        serveur.prendreCommande(commande, new RestaurantBuilder().build(), new Franchise());

        // ALORS son chiffre d'affaires est le montant de celle-ci
        assertEquals(commande.getMontant(), serveur.getChiffreAffaires());
    }

    @Test
    @DisplayName("ÉTANT DONNÉ un serveur ayant déjà pris une commande " +
            "QUAND il prend une nouvelle commande " +
            "ALORS son chiffre d'affaires est la somme des deux commandes")
    void getCaServeurPlusieursCommandes() {
        // ÉTANT DONNÉ un serveur ayant déjà pris une commande
        Serveur serveur = new ServeurBuilder().build();
        Commande premiereCommande = new CommandeBuilder()
                            .montant(15)
                            .isNourriture(true)
                            .build();
        serveur.prendreCommande(premiereCommande, new RestaurantBuilder().build(), new Franchise());

        // QUAND il prend une nouvelle commande
        Commande secondeCommande = new CommandeBuilder()
                            .montant(10)
                            .isNourriture(true)
                            .build();
        serveur.prendreCommande(secondeCommande, new RestaurantBuilder().build(), new Franchise());

        // ALORS son chiffre d'affaires est la somme des deux commandes
        double totalCommandes = premiereCommande.getMontant() + secondeCommande.getMontant();
        assertEquals(totalCommandes, serveur.getChiffreAffaires());
    }


    // Restaurant


    @ParameterizedTest
    @ValueSource(ints = {0, 1, 2, 100})
    @DisplayName("ÉTANT DONNÉ un restaurant ayant X serveurs " +
            "QUAND tous les serveurs prennent une commande d'un montant Y " +
            "ALORS le chiffre d'affaires de la franchise est X * Y " +
            "CAS(X = 0; X = 1; X = 2; X = 100) " +
            "Cas(Y = 1.0)")
    void getCaFranchise(int x) {
        double y = 1.0;

        // ÉTANT DONNÉ un restaurant ayant X serveurs
        List<Serveur> serveurs = new ServeurGenerator().Generate(x);

        // QUAND tous les serveurs prennent une commande d'un montant Y
        Commande commande = new CommandeBuilder()
                            .montant(y)
                            .isNourriture(true)
                            .build();
        Franchise franchise = new Franchise();
        for (Serveur serveur : serveurs) {
            serveur.prendreCommande(commande, new RestaurantBuilder().build(), franchise);
        }

        // ALORS le chiffre d'affaires de la franchise est X * Y
        double chiffreAffairesTotal = x * y;
        assertEquals(chiffreAffairesTotal, franchise.getChiffreAffaires());
    }


    // Franchise


    @ParameterizedTest
    @ValueSource(ints = {0, 1, 2, 1000})
    @DisplayName("ÉTANT DONNÉ une franchise ayant X restaurants de Y serveurs chacuns " +
            "QUAND tous les serveurs prennent une commande d'un montant Z " +
            "ALORS le chiffre d'affaires de la franchise est X * Y * Z" +
            "CAS(X = 0; X = 1; X = 2; X = 1000) " +
            "CAS(Y = 0; Y = 1; Y = 2; Y = 1000) " +
            "CAS(Z = 1.0)")
    void getCaFranchisePlusieursRestaurants(int x) {
        double z = 1.0;

        // ÉTANT DONNÉ une franchise ayant X restaurants de Y serveurs chacuns
        Franchise franchise = new Franchise();
        for(int i = 0; i < x; i++) {
            Restaurant restaurant = new RestaurantBuilder().build();
            for(int j = 0; j < x; j++) {
                restaurant.ajoutServeur(new ServeurBuilder().build());
            }
            franchise.ajouteRestaurant(restaurant);
        }

        // QUAND tous les serveurs prennent une commande d'un montant Z
        Commande commande = new CommandeBuilder()
                            .montant(z)
                            .isNourriture(true)
                            .build();
        for (Restaurant restaurant : franchise.getRestaurantList()) {
            for (Serveur serveur : restaurant.getServeurs()) {
                serveur.prendreCommande(commande, restaurant, franchise);
            }
        }

        // ALORS le chiffre d'affaires de la franchise est X * Y * Z
        double chiffreAffairesTotal = x * x * z;
        assertEquals(chiffreAffairesTotal, franchise.getChiffreAffaires());
    }
}
