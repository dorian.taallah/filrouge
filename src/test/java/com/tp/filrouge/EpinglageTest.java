package com.tp.filrouge;

import com.tp.filrouge.model.Commande;
import com.tp.filrouge.model.Franchise;
import com.tp.filrouge.model.Restaurant;
import com.tp.filrouge.model.Serveur;
import com.tp.filrouge.model.builder.*;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class EpinglageTest {

    @Test
    @DisplayName("ÉTANT DONNE un serveur ayant pris une commande" +
            "QUAND il la déclare comme non-payée" +
            "ALORS cette commande est marquée comme épinglée")
    void commandeEpinglee() {
        // ÉTANT DONNE un serveur ayant pris une commande
        Serveur serveur = new ServeurBuilder().build();
        Commande commande = new CommandeBuilder()
                            .montant(10)
                            .isNourriture(true)
                            .build();
        serveur.prendreCommande(commande, new RestaurantBuilder().build(), new Franchise());

        // QUAND il la déclare comme non-payée
        commande.epingler();

        // ALORS cette commande est marquée comme épinglée
        assertTrue(commande.isEpinglee());
    }

    @Test
    @DisplayName("ÉTANT DONNE un serveur ayant épinglé une commande" +
            "QUAND elle date d'il y a au moins 15 jours" +
            "ALORS cette commande est marquée comme à transmettre gendarmerie")
    void commandetoGendarmerie() {
        // ÉTANT DONNE un serveur ayant épinglé une commande
        Serveur serveur = new ServeurBuilder().build();
        Commande commande = new CommandeBuilder()
                            .montant(10)
                            .isNourriture(true)
                            .build();
        serveur.prendreCommande(commande, new RestaurantBuilder().build(), new Franchise());
        commande.epingler();

        // QUAND elle date d'il y a au moins 15 jours
        LocalDateTime localDateTime = LocalDateTime.now().minusDays(16);
        Date date = Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());
        commande.setDate(date);

        // ALORS cette commande est marquée comme à transmettre gendarmerie
        assertTrue(commande.toGendarmerie());
    }

    @Test
    @DisplayName("ÉTANT DONNE une commande à transmettre gendarmerie" +
            "QUAND on consulte la liste des commandes à transmettre du restaurant" +
            "ALORS elle y figure")
    void getCommandesATransmettre() {
        // ÉTANT DONNE une commande à transmettre gendarmerie
        Restaurant restaurant = new RestaurantBuilder().build();
        Commande commande = new CommandeBuilder()
                            .montant(10)
                            .isNourriture(true)
                            .build();
        LocalDateTime localDateTime = LocalDateTime.now().minusDays(16);
        Date date = Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());
        commande.epingler();
        commande.setDate(date);
        Serveur serveur = new ServeurBuilder().build();
        serveur.prendreCommande(commande, restaurant, new Franchise());

        // QUAND on consulte la liste des commandes à transmettre du restaurant
        List<Commande> commandesATransmettre = restaurant.getCommandesATransmettre();

        // ALORS elle y figure
        assertTrue(commandesATransmettre.contains(commande));
    }

    @Test
    @DisplayName("ÉTANT DONNE une commande à transmettre gendarmerie" +
            "QUAND elle est marquée comme transmise à la gendarmerie" +
            "ALORS elle ne figure plus dans la liste des commandes à transmettre du restaurant")
    void commandeTransmise() {
        // ÉTANT DONNE une commande à transmettre gendarmerie
        Restaurant restaurant = new RestaurantBuilder().build();
        Commande commande = new CommandeBuilder()
                            .montant(10)
                            .isNourriture(true)
                            .build();
        LocalDateTime localDateTime = LocalDateTime.now().minusDays(16);
        Date date = Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());
        commande.epingler();
        commande.setDate(date);
        Serveur serveur = new ServeurBuilder().build();
        serveur.prendreCommande(commande, restaurant, new Franchise());

        // QUAND elle est marquée comme transmise à la gendarmerie
        commande.setTransmise();

        // ALORS elle ne figure plus dans la liste des commandes à transmettre du restaurant
        List<Commande> commandesATransmettre = restaurant.getCommandesATransmettre();
        assertFalse(commandesATransmettre.contains(commande));
    }
}
