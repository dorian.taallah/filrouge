package com.tp.filrouge;

import com.tp.filrouge.model.Franchise;
import com.tp.filrouge.model.Plat;
import com.tp.filrouge.model.Restaurant;
import com.tp.filrouge.model.builder.PlatBuilder;
import com.tp.filrouge.model.builder.RestaurantBuilder;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;


class MenuTest {

    @Test
    @DisplayName("ÉTANT DONNE un restaurant ayant le statut de filiale d'une franchise ET une franchise définissant un menu ayant un plat" +
            "QUAND la franchise modifie le prix du plat" +
            "ALORS le prix du plat dans le menu du restaurant est celui défini par la franchise")
    void updatePrixPlat() {
        // ÉTANT DONNE un restaurant ayant le statut de filiale d'une franchise ET une franchise définissant un menu ayant un plat
        Franchise franchise = new Franchise();
        Plat plat = new PlatBuilder()
                .nom("burger")
                .prix(10)
                .build();
        franchise.getMenu().ajouterPlat(plat);
        Restaurant restaurant = new RestaurantBuilder().build();
        franchise.ajouteRestaurant(restaurant);

        // QUAND la franchise modifie le prix du plat
        franchise.getMenu().modifierPrixPlat(plat.getNom(), 15.0);

        // ALORS le prix du plat dans le menu du restaurant est celui défini par la franchise
        double prixRestaurant = restaurant.getMenu().getPlat(plat.getNom()).getPrix();
        double prixFranchise = franchise.getMenu().getPlat(plat.getNom()).getPrix();
        assertEquals(prixRestaurant, prixFranchise);
    }

    @Test
    @DisplayName("ÉTANT DONNE un restaurant appartenant à une franchise et définissant un menu ayant un plat ET une franchise définissant un menu ayant le même plat" +
            "QUAND la franchise modifie le prix du plat" +
            "ALORS le prix du plat dans le menu du restaurant reste inchangé")
    void updatePlat2Franchises() {
        // ÉTANT DONNE un restaurant appartenant à une franchise et définissant un menu ayant un plat ET une franchise définissant un menu ayant le même plat
        Franchise franchise = new Franchise();
        Plat plat = new PlatBuilder()
                .nom("burger")
                .prix(10)
                .build();
        franchise.getMenu().ajouterPlat(plat);
        Restaurant restaurant = new RestaurantBuilder().build();
        franchise.ajouteRestaurant(restaurant);

        Franchise franchise2 = new Franchise();
        franchise2.getMenu().ajouterPlat(plat);

        // QUAND la franchise modifie le prix du plat
        franchise2.getMenu().modifierPrixPlat(plat.getNom(), 49.99);

        // ALORS le prix du plat dans le menu du restaurant reste inchangé
        assertEquals(restaurant.getMenu().getPlat(plat.getNom()).getPrix(), plat.getPrix());
    }

    @Test
    @DisplayName("ÉTANT DONNE un restaurant appartenant à une franchise et définissant un menu ayant un plat" +
            "QUAND la franchise ajoute un nouveau plat" +
            "ALORS la carte du restaurant propose le premier plat au prix du restaurant et le second au prix de la franchise")
    void ajoutPlat() {
        // ÉTANT DONNE un restaurant appartenant à une franchise et définissant un menu ayant un plat
        Franchise franchise = new Franchise();
        Plat plat = new PlatBuilder()
                .nom("burger")
                .prix(10)
                .build();
        franchise.getMenu().ajouterPlat(plat);
        Restaurant restaurant = new RestaurantBuilder().build();
        franchise.ajouteRestaurant(restaurant);

        // QUAND la franchise ajoute un nouveau plat
        Plat salade = new PlatBuilder()
                .nom("Salade")
                .prix(5)
                .build();
        franchise.getMenu().ajouterPlat(salade);

        // ALORS la carte du restaurant propose le premier plat au prix du restaurant et le second au prix de la franchise
        assertEquals(restaurant.getMenu().getPlat(plat.getNom()).getPrix(), plat.getPrix());
        assertEquals(restaurant.getMenu().getPlat(salade.getNom()).getPrix(), salade.getPrix());
    }
}
