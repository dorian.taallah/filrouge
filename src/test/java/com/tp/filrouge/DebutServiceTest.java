package com.tp.filrouge;

import com.tp.filrouge.model.*;
import com.tp.filrouge.model.builder.*;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class DebutServiceTest {

    @Test
    @DisplayName("ÉTANT DONNE un restaurant ayant 3 tables " +
            "QUAND le service commence " +
            "ALORS elles sont toutes affectées au Maître d'Hôtel")
    void affectationMaitreHotel() {
        // ÉTANT DONNE un restaurant ayant 3 tables
        List<Table> tables = new ArrayList<>();
        for(int i = 0; i < 3; i++) {
            tables.add(new Table());
        }
        Restaurant restaurant = new RestaurantBuilder().ajoutTables(tables).build();

        // QUAND le service commence
        restaurant.debuterService();

        // ALORS elles sont toutes affectées au Maître d'Hôtel
        assertEquals(restaurant.getMaitreHotel().getTables(), tables);
    }

    @Test
    @DisplayName("ÉTANT DONNÉ un restaurant ayant 3 tables dont une affectée à un serveur " +
            "QUAND le service débute " +
            "ALORS la table éditée est affectée au serveur et les deux autres au maître d'hôtel")
    void affectationsServeurEtMaitreHotel() {
        // ÉTANT DONNÉ un restaurant ayant 3 tables dont une affectée à un serveur
        List<Table> tablesServeur = new ArrayList<>();
        tablesServeur.add(new Table());
        Serveur serveur = new ServeurBuilder().ajoutTables(tablesServeur).build();
        List<Table> tablesMaitreHotel = new ArrayList<>();
        for(int i = 0; i < 2; i++) {
            tablesMaitreHotel.add(new Table());
        }

        Restaurant restaurant = new RestaurantBuilder()
        		.ajoutServeur(serveur)
        		.ajoutTables(tablesMaitreHotel)
        		.build();


        // QUAND le service débute
        restaurant.debuterService();

        // ALORS la table éditée est affectée au serveur et les deux autres au maître d'hôtel
        assertTrue(restaurant.getServeurs().get(restaurant.getServeurs().indexOf(serveur)).getTables().contains(tablesServeur.get(0)));
        assertTrue(restaurant.getMaitreHotel().getTables().containsAll(tablesMaitreHotel));
    }

    @Test
    @DisplayName("ÉTANT DONNÉ un restaurant ayant 3 tables dont une affectée à un serveur " +
            "QUAND le service débute " +
            "ALORS il n'est pas possible de modifier le serveur affecté à la table")
    void modifServeurApresDebutService() {
        // ÉTANT DONNÉ un restaurant ayant 3 tables dont une affectée à un serveur
        List<Table> tablesServeur = new ArrayList<>();
        tablesServeur.add(new Table());
        Serveur serveur = new ServeurBuilder().ajoutTables(tablesServeur).build();
        List<Table> tablesMaitreHotel = new ArrayList<>();
        for(int i = 0; i < 2; i++) {
            tablesMaitreHotel.add(new Table());
        }
        
        Restaurant restaurant = new RestaurantBuilder()
        		.ajoutServeur(serveur)
        		.ajoutTables(tablesMaitreHotel)
        		.build();


        // QUAND le service débute
        restaurant.debuterService();

        // ALORS il n'est pas possible de modifier le serveur affecté à la table
        assertFalse(restaurant.annulerTableServeur(tablesServeur.get(0), serveur));
    }

    @Test
    @DisplayName("ÉTANT DONNÉ un restaurant ayant 3 tables dont une affectée à un serveur ET ayant débuté son service" +
            "QUAND le service se termine ET qu'une table est affectée à un serveur" +
            "ALORS la table éditée est affectée au serveur et les deux autres au maître d'hôtel")
    void serviceTermineAffectationTables() {
        // ÉTANT DONNÉ un restaurant ayant 3 tables dont une affectée à un serveur ET ayant débuté son service
        List<Table> tablesServeur = new ArrayList<>();
        tablesServeur.add(new Table());
        Serveur serveur = new ServeurBuilder().ajoutTables(tablesServeur).build();
        List<Table> tablesMaitreHotel = new ArrayList<>();
        for(int i = 0; i < 2; i++) {
            tablesMaitreHotel.add(new Table());
        }
        
        Restaurant restaurant = new RestaurantBuilder()
        		.ajoutServeur(serveur)
        		.ajoutTables(tablesMaitreHotel)
        		.ajoutTables(tablesServeur, serveur)
        		.build();

        restaurant.debuterService();

        // QUAND le service se termine ET qu'une table est affectée à un serveur
        restaurant.terminerService();
        restaurant.ajoutTables(tablesServeur, serveur);

        // ALORS la table éditée est affectée au serveur et les deux autres au maître d'hôtel
        assertTrue(restaurant.getServeurs().get(restaurant.getServeurs().indexOf(serveur)).getTables().contains(tablesServeur.get(0)));
        assertTrue(restaurant.getMaitreHotel().getTables().containsAll(tablesMaitreHotel));
    }
}
