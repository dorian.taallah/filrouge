package com.tp.filrouge;

import com.tp.filrouge.model.Restaurant;
import com.tp.filrouge.model.Table;
import com.tp.filrouge.model.builder.RestaurantBuilder;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class InstallationClientTest {

    @Test
    @DisplayName("ÉTANT DONNE une table dans un restaurant ayant débuté son service " +
                "QUAND un client est affecté à une table " +
                "ALORS cette table n'est plus sur la liste des tables libres du restaurant")
    void affectationClient() {
        // ÉTANT DONNE une table dans un restaurant ayant débuté son service
        Table table = new Table();
        
        Restaurant restaurant = new RestaurantBuilder()
        		.ajoutTable(table)
        		.build();
        restaurant.debuterService();

        // QUAND un client est affecté à une table
        table.installerClient();

        // ALORS cette table n'est plus sur la liste des tables libres du restaurant
        assertFalse(restaurant.tableEstLibre(table));
    }

    @Test
    @DisplayName("ÉTANT DONNE une table occupée par un client " +
                "QUAND la table est libérée " +
                "ALORS cette table appraît sur la liste des tables libres du restaurant")
    void desaffectationClient() {
        // ÉTANT DONNE une table occupée par un client
        Table table = new Table();
        Restaurant restaurant = new RestaurantBuilder().ajoutTable(table).build();

        restaurant.debuterService();
        table.installerClient();

        // QUAND la table est libérée
        table.libererClient();

        // ALORS cette table appraît sur la liste des tables libres du restaurant
        assertTrue(restaurant.tableEstLibre(table));
    }
}
