package com.tp.filrouge;

import com.tp.filrouge.model.Commande;
import com.tp.filrouge.model.Franchise;
import com.tp.filrouge.model.Restaurant;
import com.tp.filrouge.model.Serveur;
import com.tp.filrouge.model.builder.*;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class CommandeTest {

    @Test
    @DisplayName("ÉTANT DONNE un serveur dans un restaurant " +
            "QUAND il prend une commande de nourriture " +
            "ALORS cette commande apparaît dans la liste de tâches de la cuisine de ce restaurant")
    void commandeNourriture() {
        // ÉTANT DONNE un serveur dans un restaurant
        Serveur serveur = new ServeurBuilder().build();
        Restaurant restaurant = new RestaurantBuilder().ajoutServeur(serveur).build();

        // QUAND il prend une commande de nourriture
        Commande commande = new CommandeBuilder()
                            .montant(10)
                            .isNourriture(true)
                            .build();
        serveur.prendreCommande(commande, restaurant, new Franchise());

        // ALORS cette commande apparaît dans la liste de tâches de la cuisine de ce restaurant
        assertTrue(restaurant.getCommandes().contains(commande));
    }

    @Test
    @DisplayName("ÉTANT DONNE un serveur dans un restaurant " +
            "QUAND il prend une commande de nourriture " +
            "ALORS cette commande n'apparaît pas dans la liste de tâches de la cuisine de ce restaurant")
    void commandeBoisson() {
        // ÉTANT DONNE un serveur dans un restaurant
        Serveur serveur = new ServeurBuilder().build();
        Restaurant restaurant = new RestaurantBuilder().ajoutServeur(serveur).build();


        // QUAND il prend une commande de nourriture
        Commande commande = new CommandeBuilder()
                            .montant(10)
                            .isNourriture(false)
                            .build();
        serveur.prendreCommande(commande, restaurant, new Franchise());

        // ALORS cette commande n'apparaît pas dans la liste de tâches de la cuisine de ce restaurant
        assertFalse(restaurant.getCommandes().contains(commande));
    }
}
