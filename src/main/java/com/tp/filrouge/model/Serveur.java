package com.tp.filrouge.model;

import java.util.ArrayList;
import java.util.List;

public class Serveur {

    private double chiffreAffaires;
    private final List<Table> tables;

    public Serveur() {
        this.chiffreAffaires = 0;
        this.tables = new ArrayList<>();
    }
    
    public Serveur(double _chiffreAffaires, List<Table> _tables) {
        this.chiffreAffaires = _chiffreAffaires;
        this.tables = _tables;
    }

    public void prendreCommande(Commande commande, Restaurant restaurant, Franchise franchise) {
        this.chiffreAffaires += commande.getMontant();
        if(commande.isNourriture()) {
            restaurant.ajoutCommande(commande, franchise);
        }
    }

    public void ajoutTables(List<Table> tables) {
        this.tables.addAll(tables);
    }

    public double getChiffreAffaires() {
        return chiffreAffaires;
    }

    public List<Table> getTables() {
        return tables;
    }

    public Table enleverTable(Table table) {
        if(this.tables.contains(table)) {
            this.tables.remove(table);
            return table;
        } else {
            return null;
        }
    }
}
