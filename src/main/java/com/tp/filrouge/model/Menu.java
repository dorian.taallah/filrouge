package com.tp.filrouge.model;

import java.util.ArrayList;
import java.util.List;

public class Menu {

    private final List<Plat> plats;

    public Menu() {
        this.plats = new ArrayList<>();
    }

    public void ajouterPlat(Plat plat) {
        this.plats.add(plat);
    }

    public void modifierPrixPlat(String nom, double prix) {
        this.plats.stream()
                .filter(p -> p.getNom().equals(nom)).findFirst().ifPresent(plat -> plat.setPrix(prix));
    }

    public Plat getPlat(String nom) {
        return this.plats.stream().filter(plat -> plat.getNom().equals(nom)).findFirst().orElse(null);
    }
}
