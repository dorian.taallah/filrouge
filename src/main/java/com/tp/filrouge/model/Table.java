package com.tp.filrouge.model;

public class Table {

    private boolean occupe;

    public Table() {
        this.occupe = false;
    }

    public boolean isOccupe() {
        return occupe;
    }

    public void installerClient() {
        this.occupe = true;
    }

    public void libererClient() {
        this.occupe = false;
    }
}
