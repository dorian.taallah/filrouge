package com.tp.filrouge.model;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

public class Commande {

    private final double montant;
    private final boolean isNourriture;
    private boolean epinglee = false;
    private Date date = new Date();

    public Commande(double montant, boolean isNourriture) {
        this.montant = montant;
        this.isNourriture = isNourriture;
    }

    public void epingler() {
        this.epinglee = true;
    }

    public double getMontant() {
        return montant;
    }

    public boolean isNourriture() {
        return isNourriture;
    }

    public boolean isEpinglee() {
        return epinglee;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public void setTransmise() {
        this.epinglee = false;
    }

    public boolean toGendarmerie() {
        LocalDateTime localDateTime = LocalDateTime.now().minusDays(15);
        Date date15 = Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());
        return this.date.before(date15);
    }
}
