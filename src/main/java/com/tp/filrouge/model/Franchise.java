package com.tp.filrouge.model;

import java.util.ArrayList;
import java.util.List;

public class Franchise {

    private double chiffreAffaires;
    private final List<Restaurant> restaurantList;
    private final Menu menu;

    public Franchise() {
        this.chiffreAffaires = 0.0;
        this.restaurantList = new ArrayList<>();
        this.menu = new Menu();
    }

    public void ajouteCommande(Commande commande) {
        this.chiffreAffaires += commande.getMontant();
    }

    public void ajouteRestaurant(Restaurant restaurant) {
        this.restaurantList.add(restaurant);
        restaurant.setMenu(this.menu);
    }

    public double getChiffreAffaires() {
        return chiffreAffaires;
    }

    public List<Restaurant> getRestaurantList() {
        return restaurantList;
    }

    public Menu getMenu() {
        return menu;
    }
}
