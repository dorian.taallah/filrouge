package com.tp.filrouge.model;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

public class Restaurant {

    private List<Table> tables = new ArrayList<>();
    private List<Serveur> serveurs = new ArrayList<>();
    private List<Commande> commandes = new ArrayList<>();
    private MaitreHotel maitreHotel = new MaitreHotel();
    private boolean serviceDebute = false;
    private Menu menu = new Menu();

    public Restaurant(List<Table> tables, List<Serveur> serveurs, List<Commande> commandes, MaitreHotel maitreHotel, boolean serviceDebute, Menu menu) {
        this.tables = tables;
        this.serveurs = serveurs;
        this.commandes = commandes;
        this.maitreHotel = maitreHotel;
        this.serviceDebute = serviceDebute;
        this.menu = menu;
    }
    
    public void debuterService() {
        this.maitreHotel.ajoutTables(this.tables);
        this.serviceDebute = true;
    }

    public boolean tableEstLibre(Table table) {
        return !table.isOccupe();
    }

    public void ajoutCommande(Commande commande, Franchise franchise) {
        this.commandes.add(commande);
        franchise.ajouteCommande(commande);
    }

    public void ajoutServeur(Serveur serveur) {
        this.serveurs.add(serveur);
    }

    public List<Serveur> getServeurs() {
        return serveurs;
    }

    public List<Commande> getCommandes() {
        return commandes;
    }

    public MaitreHotel getMaitreHotel() {
        return maitreHotel;
    }

    public void ajoutTables(List<Table> tables) {
        this.tables.addAll(tables);
    }

    public void ajoutTables(List<Table> tables, Serveur serveur) {
        serveur.ajoutTables(tables);
    }

    public boolean annulerTableServeur(Table table, Serveur serveur) {
        if(!serviceDebute) {
            Table tableRetiree = serveur.enleverTable(table);
            if(tableRetiree != null) {
                this.tables.add(tableRetiree);
                return true;
            }
        }
        return false;
    }

    public void terminerService() {
        this.serviceDebute = false;
        List<Table> tablesRetirees = new ArrayList<>();
        for(Serveur serveur : this.serveurs) {
            for(int i = 0; i < serveur.getTables().size(); i++) {
                tablesRetirees.add(serveur.enleverTable(serveur.getTables().get(i)));
            }
        }
        this.tables = tablesRetirees;
    }

    public List<Commande> getCommandesATransmettre() {
        LocalDateTime localDateTime = LocalDateTime.now().minusDays(15);
        Date date15 = Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());
        return this.commandes.stream()
                .filter(commande -> commande.isEpinglee() && commande.getDate().before(date15))
                .collect(Collectors.toList());
    }

    public Menu getMenu() {
        return menu;
    }

    public void setMenu(Menu menu) {
        this.menu = menu;
    }
}
