package com.tp.filrouge.model.builder;

import java.util.ArrayList;
import java.util.List;

import com.tp.filrouge.model.Commande;
import com.tp.filrouge.model.MaitreHotel;
import com.tp.filrouge.model.Menu;
import com.tp.filrouge.model.Restaurant;
import com.tp.filrouge.model.Serveur;
import com.tp.filrouge.model.Table;


public class RestaurantBuilder {
    private List<Table> tables = new ArrayList<>();
    private List<Serveur> serveurs = new ArrayList<>();
    private List<Commande> commandes = new ArrayList<>();
    private MaitreHotel maitreHotel = new MaitreHotel();
    private boolean serviceDebute = false;
    private Menu menu = new Menu();
	
	public RestaurantBuilder ajoutTables(List<Table> _tables) {
		this.tables = _tables;
		return this;
	}
	
	public RestaurantBuilder ajoutTable(Table _table) {
		this.tables.add(_table);
		return this;
	}
	
    public RestaurantBuilder ajoutTables(List<Table> tables, Serveur serveur) {
        serveur.ajoutTables(tables);
        return this;
    }
	
	public RestaurantBuilder ajoutCommandes(List<Commande> _commandes) {
		this.commandes = _commandes;
		return this;
	}
	
	public RestaurantBuilder ajoutCommande(Commande _commande) {
		this.commandes.add(_commande);
		return this;
	}
	
	public RestaurantBuilder ajoutServeurs(List<Serveur> _serveurs) {
		this.serveurs.addAll(_serveurs);
		return this;
	}
	
	public RestaurantBuilder ajoutServeur(Serveur _serveur) {
		this.serveurs.add(_serveur);
		return this;
	}
	
	public RestaurantBuilder ajoutMaitreHotel(MaitreHotel _maitreHotel) {
		this.maitreHotel = _maitreHotel;
		return this;
	}
	
	public RestaurantBuilder serviceDebute(boolean _serviceDebute) {
		this.serviceDebute = _serviceDebute;
		return this;
	}
	
	public RestaurantBuilder ajoutMenu(Menu _menu) {
		this.menu = _menu;
		return this;
	}
	
	public Restaurant build() {
		Restaurant restaurant = new Restaurant(this.tables, this.serveurs, this.commandes, this.maitreHotel, this.serviceDebute, this.menu);
		return restaurant;
	}
}
