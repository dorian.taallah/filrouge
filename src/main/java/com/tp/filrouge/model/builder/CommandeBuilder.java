package com.tp.filrouge.model.builder;

import com.tp.filrouge.model.Commande;

public class CommandeBuilder
	{
		private double montant;
		private boolean isNourriture;

		public CommandeBuilder() {}

		public CommandeBuilder montant(double montant) {
			this.montant = montant;
			return this;
		}

		public CommandeBuilder isNourriture(boolean isNourriture) {
			this.isNourriture = isNourriture;
			return this;
		}

		//Return the finally consrcuted User object
		public Commande build() {
			Commande user =  new Commande(montant,isNourriture);
			return user;
		}
	}