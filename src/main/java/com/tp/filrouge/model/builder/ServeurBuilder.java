package com.tp.filrouge.model.builder;

import java.util.List;

import com.tp.filrouge.model.Serveur;
import com.tp.filrouge.model.Table;

public class ServeurBuilder {
    private double chiffreAffaires;
    private List<Table> tables;
	
	public ServeurBuilder ajoutChiffreAffaires(double _chiffreAffaires) {
		this.chiffreAffaires = _chiffreAffaires;
		return this;
	}
    
	public ServeurBuilder ajoutTable(Table _table) {
		this.tables.add(_table);
		return this;
	}
	
	public ServeurBuilder ajoutTables(List<Table> _tables) {
		this.tables = _tables;
		return this;
	}
    
	public Serveur build() {
		Serveur serveur = new Serveur(this.chiffreAffaires, this.tables);
		return serveur;
	}
}
