package com.tp.filrouge.model.builder;

import java.util.ArrayList;
import com.tp.filrouge.model.Serveur;
import java.util.List;

public class ServeurGenerator {
    private ServeurBuilder _builder = new ServeurBuilder();
    
    public List<Serveur> Generate(int nb)
    {
    	List<Serveur> serveurs = new ArrayList<>();
        for (var i = 0; i < nb; i++)
        {
        	serveurs.add(_builder.build());
        }
        return serveurs;
    }
    
}
