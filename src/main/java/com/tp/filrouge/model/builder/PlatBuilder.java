package com.tp.filrouge.model.builder;
import com.tp.filrouge.model.Plat;


public class PlatBuilder {

    private String nom;
    private double prix;

    public PlatBuilder() {}

    public PlatBuilder nom(String nom) {
        this.nom = nom;
        return this;
    }

    public PlatBuilder prix(double prix) {
        this.prix = prix;
        return this;
    }

    //Return the finally consrcuted User object
    public Plat build() {
        Plat user =  new Plat(nom,prix);
        return user;
    }
}
